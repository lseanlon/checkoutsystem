

export enum ProductEnum {
    classic = 1,
    standout = 2,
    premium = 3
}


export const ProductTitleConst = {
    'classic': 'Classic Ad',
    'standout': 'Standout Ad',
    'premium': 'Premium Ad ',
} 

export const ProductDescriptionConst = {
    'classic': 'Offers the most basic level of advertisement',
    'standout': 'Allows advertisers to use a company logo and use a longer presentation text ',
    'premium': ' Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility ',
}

export const ProductPreviewConst = {
    'classic': 'classicPreview.gif',
    'standout': 'standoutPreview.gif',
    'premium': 'premiumPreview.gif',
}

export const ProductLogoConst = {
    'classic': 'car',
    'standout': 'plane',
    'premium': 'fighter-jet',
}

  