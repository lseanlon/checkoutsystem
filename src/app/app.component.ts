import { Component } from '@angular/core';

import { EnvironmentEnum } from './enum/Environment.enum';
import { ILoader } from './interface/ILoader';
import { ApiService } from './service/Api.service';
import { MessageService } from './service/Message.service';


import { environment } from '../../src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss', './component/material.component.scss', './component/common.component.scss']
})

export class AppComponent {
    title = 'Checkout System';
    notReadyClass = 'not-ready';
    currentTheme: string;
    environmentType: string;
    appVersion: string;
    apiLoader: ILoader;
    formLoader: ILoader;


    constructor(private apiService: ApiService,
        private messageService: MessageService) {
        this.configureApi();
        this.configureLogger();
        this.apiLoader = this.apiService.loader;
        this.environmentType = environment.targetEnvironment;
    }

//allow deployment-independent configuration to access api
    configureApi = () => {
        this.apiService.setfullUrlPath('./assets/data/api-config.json?t=' + Date.now());
        this.apiService.getData()
            .then(data => {
                console.log('Successful in getting apiurl');
                this.apiService.setBaseUrlPath(data['storageApiUrl']);
                this.apiService.setAuthUrlPath(data['authApiUrl']);

            })
            .catch(error => { console.log('Error in getting apiurl', error) });

    }
    configureLogger = () => {

        let logger = console;
        //dev and ci
        if (EnvironmentEnum.dev == EnvironmentEnum[this.environmentType] || EnvironmentEnum.ci == EnvironmentEnum[this.environmentType]) {
            logger = console;
        }

        // pre and prod
        if (EnvironmentEnum.pp == EnvironmentEnum[this.environmentType] || EnvironmentEnum.prod == EnvironmentEnum[this.environmentType]) {
            logger = null;
        }
        window.alert = (messages) => {
            return logger ? alert(messages) : null;
        };
        (<any>window).console = <any>{
            log: (messages) => {
                return logger ? logger.log(messages) : null;
            },
            warn: (messages) => {
                return logger ? logger.warn(messages) : null;
            },
            info: (messages) => {
                return logger ? logger.info(messages) : null;
            },
            error: (messages) => {
                return logger ? logger.error(messages) : null;
            }

        };

    }

    ngOnInit(): void {
        this.appVersion = environment.version;
    }


    performLoad(): void {
        this.notReadyClass = '';
    }

    setMessage(): void { }
    initializeValue(): void { }




}


