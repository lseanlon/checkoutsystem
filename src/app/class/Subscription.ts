export class Subscription {
  id: number;
  skuCode: string;
  unit: string;
  isActive: boolean;
  startDate: string;
  expiryDate: string;
 
}
 