
import { DiscountTrigger } from './Discount/DiscountTrigger';
import { DiscountResult } from './Discount/DiscountResult';
export class PricingRule {
  id: number;
  discountTrigger: DiscountTrigger;
  discountResult: DiscountResult[];
}
