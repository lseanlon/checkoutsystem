export class DiscountResult    {
  id: number;
  skuCode: string; 
  measurement: string;//unit or amount
  unitValue: number;
  totalValue: string; 
}
 
//eg. [{ "skuScope" :"CLASSIC", "measurement":"unit",  "unitValue": 0,"totalValue":"-1"  }]