export class DiscountTrigger    {
  id: number;
  skuCode: string; 
  measurement: string; //unit or amount
  condition: string; //eg. >,>=
  value: string; 
  description: string; 
  expiryDate: string; 
}
 
 