import { Item } from './Item';
import { PricingRule } from './PricingRule';


export class Checkout {
  id: number;
  items: Item[]; 
  pricingRules:PricingRule[] ; 
}
 