import { Subscription } from './Subscription';

export class UserProfile {
  id: number;
  username: string;
  password: string;
  publicName: string;
  isLoggedIn: boolean;
  sessionKey: string;
  profileUrl: string;
  subscriptionList: Subscription[]; 

  creditCard: string;
  creditCardBank: string;
}
 

 