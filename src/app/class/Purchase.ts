import { Subscription } from './Subscription';

export class Purchase {
  skuCode: string;
  price: number; 
  startDate: string;
  expiryDate: string;
}
 
 