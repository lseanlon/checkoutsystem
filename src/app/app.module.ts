import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HeaderComponent } from './component/header/header.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';

import { ApiService } from './service/Api.service';
import { SessionService } from './service/Session.service';
import { CartService } from './service/Cart.service';
 
import { MessageService } from './service/Message.service';
import { ProductInfo } from './pipe/Product.pipe';  
import { APP_BASE_HREF } from '@angular/common';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';

@NgModule({
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpModule],
  declarations: [AppComponent, HeaderComponent, ProductInfo ],
  bootstrap: [AppComponent],
  providers: [ApiService, SessionService,CartService,
    MessageService, { provide: LocationStrategy, useValue: '/', useClass: HashLocationStrategy }]
})
export class AppModule { }
