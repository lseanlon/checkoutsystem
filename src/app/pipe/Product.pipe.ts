import { Pipe, PipeTransform } from '@angular/core';
import { ProductEnum } from '../enum/Product.enum';
import { ProductTitleConst } from '../enum/Product.enum';
import { ProductDescriptionConst } from '../enum/Product.enum';
import { ProductPreviewConst } from '../enum/Product.enum';
import { ProductLogoConst } from '../enum/Product.enum';

@Pipe({ name: 'productInfo' })
export class ProductInfo implements PipeTransform {

    transform(value: string, valueType: any): string {
        if (!value) { return ''; }
        if (!ProductEnum[value]) { return ''; }

        if (valueType == 'title') { return ProductTitleConst[value]; }
        if (valueType == 'description') { return ProductDescriptionConst[value]; }
        if (valueType == 'preview') { return ProductPreviewConst[value]; }
        if (valueType == 'logo') { return ProductLogoConst[value]; }


    }
}
