import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { UserProfile } from '../class/UserProfile';
import { Purchase } from '../class/Purchase';
import { Checkout } from '../class/Checkout/Checkout';
import { Item } from '../class/Checkout/Item';
import { PricingRule } from '../class/Checkout/PricingRule';
import { ApiService } from './Api.service';
import { MessageService } from './Message.service';


@Injectable()
export class CartService {

    constructor(private http: Http, private apiService: ApiService, private messageService: MessageService) {
    }

    private currentCheckout: Checkout = new Checkout();
    private currentPricingRule: PricingRule;


    getCurrentCheckout(): Checkout {
        return this.currentCheckout;
    }

    setCurrentCheckout(currentCheckout: Checkout) {
        this.currentCheckout = currentCheckout;
    }


    initializeItem(items: Item[]) {
        this.currentCheckout.items = items;
    }
    initializePricingRule(pricingRule: PricingRule[]) {
        this.currentCheckout.pricingRules = pricingRule;
    }

    addCurrentItem(skuItem: Item) {
        this.currentCheckout.items = this.currentCheckout.items || [];
        this.currentCheckout.items.push(skuItem);
    }

    removeCurrentItem(skuCode: string) {

        const itemIndex = this.currentCheckout.items.findIndex(curElem => curElem.skuCode === skuCode);
        //remove from current list
        this.currentCheckout.items[itemIndex] = null;
        this.currentCheckout.items = this.currentCheckout.items.filter(function (element) {
            return element;
        });
    }

    getCurrentPricingRule(): PricingRule {
        return this.currentPricingRule;
    }

    setCurrentPricingRule(currentPricingRule: PricingRule) {
        this.currentPricingRule = currentPricingRule;
    }

}
