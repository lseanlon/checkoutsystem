import { Injectable } from '@angular/core';

import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { ILoader } from '../interface/ILoader';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
    public loader: ILoader = { isLoading: false, networkMessage: '' };

    // URL to web api
    // http://localhost:3000/comments/2
    // private baseUrlPath = 'api/storage';
    private baseUrlPath: string; // = environment.baseApiUrl;
    private authUrlPath: string; // = environment.authApiUrl;
    private fullUrlPath: string;

    private bearerTokenPrefix = 'bearer';
    private bearerToken;
    private bearerResponse = {};

    //TODO: not all of these keys should be sent for each request!
    private headers = new Headers({
        'Content-Type': 'application/json',
        'X-ScS-Key-Token': environment.secretKey,
        'Authorization': `${this.bearerTokenPrefix} ${this.bearerToken}`
    });

    private bodyRequest = {};
    private bodyResponse = {};


    constructor(private http: Http) {

    }

    showLoader() {
        this.loader.isLoading = true;
    }
    hideLoader() {
        this.loader.isLoading = false;
    }

    getBearerResponse() {
        return this.bearerResponse;
    }
    setBearerResponse(bearerResponse) {
        this.bearerResponse = bearerResponse;
        if (bearerResponse) {
            this.bearerTokenPrefix = this.bearerResponse['token_type'];
            this.bearerToken = this.bearerResponse['access_token'];
            const authorizationToken = `${this.bearerTokenPrefix} ${this.bearerToken}`;
            //update header token
            this.updateHeaderKey('Authorization', authorizationToken);
        }
    }

    updateHeaderKey(headerKeyName: string, headerKeyValue: string) {
        this.headers.set(headerKeyName, headerKeyValue);
    }
    removeHeaderKey(headerKeyName) {
        const currentHeaderJson = this.headers.toJSON();
        delete currentHeaderJson[headerKeyName];
        this.headers = new Headers(currentHeaderJson);
    }
    //request
    getBodyRequest(): any {
        return this.bodyRequest;
    }
    setBodyRequest(bodyRequest) {
        this.bodyRequest = bodyRequest;
    }
    //response
    getBodyResponse(): any {
        return this.bodyResponse;
    }
    setBodyResponse(bodyResponse) {
        this.bodyResponse = bodyResponse;
    }

    //baseUrlPath
    getBaseUrlPath(): string {
        return this.baseUrlPath;
    }
    setBaseUrlPath(baseUrlPath: string) {
        this.baseUrlPath = baseUrlPath;
    }

    //authUrlPath
    getAuthUrlPath(): string {
        return this.authUrlPath;
    }
    setAuthUrlPath(authUrlPath: string) {
        this.authUrlPath = authUrlPath;
    }

    //baseUrlPath
    getfullUrlPath(): string {
        return this.fullUrlPath;
    }
    setfullUrlPath(fullUrlPath): void {
        this.fullUrlPath = fullUrlPath;
    }
    //TODO:prefix should be renamed to endpoint -> technically this is postfix, not prefix!
    setServicePath(prefix: string): void {
        this.fullUrlPath = `${this.baseUrlPath}${prefix}`;
    }
    setServiceAuthPath(endpoint: string) {
        this.fullUrlPath = `${this.authUrlPath}${endpoint}`;
    }
    performNetworkLogger(resp) {
        const responseMessage = JSON.stringify({ 'body': resp.json(), 'ok': resp.ok, 'statusText': resp.statusText, 'status': resp.status, 'url': resp.url, 'type': resp.type });
        this.loader.networkMessage = responseMessage;
        console.log('networkMessage ', responseMessage);

    }

    getData(): Promise<any> {

        this.showLoader();
        return this.http.get(this.fullUrlPath, { headers: this.headers }).toPromise()
            .then((response) => {
                this.hideLoader();
                this.bodyResponse = response.json() as any[];
                console.log('response.json()', JSON.stringify(response.json()));
                this.performNetworkLogger(response);
                return this.bodyResponse;
            })
            .catch((e) => {
                this.hideLoader();
                this.handleError(e)
                console.log('getData() e', e);
            })
    }

    getMultipleData(requestServiceName: any[], requestUrlPath: any[]): Observable<any> {

        const formatRequestUrlPath = [];

        //format list of url into list of get-promises
        requestUrlPath.forEach(urlData => {
            const fullUrlPath = this.baseUrlPath + urlData;
            formatRequestUrlPath.push(this.http.get(fullUrlPath, { headers: this.headers }).map((res: Response) => res.json()));
        });


        //loader will be handled by subscriber because need account loading field time.
        return Observable.forkJoin(formatRequestUrlPath).map((data: any[]) => {
            console.log('networkMessage ', data);
            return data;
        }).catch(err => { console.log('getMultipleData error ', err); return err; });
    }


    handleError(errorObject: any): Promise<any> {
        console.info('An error occurred', errorObject);
        if (this) { 
            this.bodyResponse = errorObject;
        }
        return Promise.reject('error ');

    }

    getDataBasedOnId(id: number): Promise<any> {

        const url = `${this.fullUrlPath}/${id}`;
        return this.http.get(url)
            .toPromise()
            // .then(response => response.json().data as any)
            .then(response => {
                this.hideLoader(); this.bodyResponse = response.json().data; return response.json().data;
            })
            .catch(this.handleError);
    }


    postData(requestObj: any): Promise<any> {

        return this.http
            .post(this.fullUrlPath, (requestObj), { headers: this.headers })
            .toPromise()
            .then(response => {
                this.hideLoader();
                this.bodyResponse = response.json() as any[];
                return this.bodyResponse;
            })
            .catch(this.handleError);
    }


    updateData(requestObj: any): Promise<any> {

        const url = `${this.fullUrlPath}/${requestObj.id}`;
        return this.http
            .put(url, JSON.stringify(requestObj), { headers: this.headers })
            .toPromise()
            .then(() => {
                requestObj;
                this.hideLoader();
            })
            .catch(this.handleError);
    }

    deleteData(requestObj: any): Promise<void> {

        const url = `${this.fullUrlPath}/${requestObj.id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => {
                requestObj;
                this.hideLoader();
            })
            .catch(this.handleError);
    }



}
