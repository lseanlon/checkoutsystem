import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { UserProfile } from '../class/UserProfile';
import { ApiService } from './Api.service';
import { MessageService } from './Message.service';


@Injectable()
export class SessionService {

    constructor(private http: Http, private apiService: ApiService, private messageService: MessageService) {
    }

    private currentUser: UserProfile;


    getCurrentUser(): UserProfile {
        if (!this.currentUser) {  
            return null;
        }
        return this.currentUser;
    }

    setCurrentUser(currentUser: UserProfile) {
        this.currentUser = currentUser;
    }


    performLogout() {
        this.messageService.announceMessage("pre-login");
        this.apiService.setServicePath(`/logout/${this.getCurrentUser().username}`);

        //@todo - in prod is put format 
        this.apiService.getData().then(response => { });  
    }

    redirectLandingPage() {
        location.href = "#/subscription";
        // location.href = "#/purchase";
    }

    performLogin(userName: string, userPassword: string) {

        if (this.currentUser && this.currentUser.isLoggedIn) {
            this.redirectLandingPage();
            return;
        }

        if (!userName || !userPassword) {
            return;
        }

        this.apiService.setServicePath(`/login/${userName}`);
        //@todo - in prod is post format with body{username,password}
        this.apiService.getData()
            .then(response => {
                this.currentUser = response;
                if (response && response.isLoggedIn) {

                    this.redirectLandingPage();
                } else {
                    alert("Failed to login. Invalid credentials")
                }
            });
        this.messageService.announceMessage("post-login");

    }

}
