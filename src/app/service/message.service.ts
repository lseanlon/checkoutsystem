import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject'; 

@Injectable()
export class MessageService {
  // Observable string sources
  private messageAnnouncedSource = new Subject<any>();
  private messageConfirmedSource = new Subject<any>();

  private formEventSubmitAnnouncedSource = new Subject<any>();
  private inputEventSelectAnnouncedSource = new Subject<any>();
  private azureResponseAnnouncedSource = new Subject<any>();
  // Observable string streams
  messageAnnounced$ = this.messageAnnouncedSource.asObservable();
  messageConfirmed$ = this.messageConfirmedSource.asObservable();
  formEventSubmitAnnounced$ = this.formEventSubmitAnnouncedSource.asObservable();
  inputEventSelectAnnounced$ = this.inputEventSelectAnnouncedSource.asObservable();

  azuereResponseAnnounced$ = this.azureResponseAnnouncedSource.asObservable();

  // Service message commands
  announceMessage(message: any) {
    this.messageAnnouncedSource.next(message);
  }

  confirmMessage(message: any) {
    this.messageConfirmedSource.next(message);
  }
  formEventSubmitMessage(formEventSubmit: any) {
    // alert("formEventSubmitMessage");
    this.formEventSubmitAnnouncedSource.next(formEventSubmit);
  }

  inputEventSelectMessage(inputEventSelect: any) {
    this.inputEventSelectAnnouncedSource.next(inputEventSelect);
  }
 

}
