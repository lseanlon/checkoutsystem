

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';
import { ProductEnum } from '../../enum/Product.enum';
import { Subscription } from '../../class/subscription';
import { Checkout } from '../../class/Checkout/Checkout';
import { DiscountTrigger } from '../../class/Checkout/Discount/DiscountTrigger';
import { DiscountResult } from '../../class/Checkout/Discount/DiscountResult';
import { PricingRule } from '../../class/Checkout/PricingRule';

import { SessionService } from '../../service/Session.service';
import { CartService } from '../../service/Cart.service';
import { ProductInfo } from '../../pipe/Product.pipe';

@Component({
    selector: 'pay-component',
    moduleId: module.id,
    templateUrl: './pay.component.html',
    styleUrls: ['pay.component.scss', '../purchase/purchase.component.scss', '../common.component.scss']
})


export class PayComponent implements OnInit {


    skuSelectionList = []; //order by sku group
    isShowingInfo: boolean = false;
    showingCategory: string = '';

    //general purchase info
    listPurchase: TransientFormat[] = [];//{skuCode, unit, amount}
    listPurchaseSummary = { "totalAmounts": 0, "totalUnits": 0 };

    //purchase info with discount
    listPurchaseDiscount: TransientFormat[] = [];
    listPurchaseSummaryDiscount = { "totalAmounts": 0, "totalUnits": 0 };; //{  transientFormat}

    discountEvaluator;

    currentSkuCode: string;
    currentCheckout: Checkout

    constructor(
        private sessionService: SessionService,
        private cartService: CartService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {

        console.log('route route', route);

    }
    ngOnInit(): void {
        this.initializeDiscountEvaluator();
        this.initializeBeforeDiscount();
        this.initializeAfterDiscount();
    }

    initializeDiscountEvaluator() {
        this.discountEvaluator = {};

        this.discountEvaluator['initialize'] = (currentFormat: TransientFormat) => {
            this.discountEvaluator['currentFormat'] = currentFormat;
        };

        this.discountEvaluator['evaluate'] = () => {
            this.discountEvaluator['currentFormat']['pricingRules'].forEach((resultData, indexData) => {
                this.discountEvaluator['evaluateTarget'](resultData.discountTrigger, resultData.discountResult)
            });

            return this.discountEvaluator['currentFormat'];
        };

        //return back new aggregated discount deduction for skus
        this.discountEvaluator['evaluateTarget'] = (discountTrigger: DiscountTrigger, discountResults: DiscountResult[]) => {
            let currentFormat = this.discountEvaluator['currentFormat'];
            if (currentFormat.skuCode == discountTrigger.skuCode) {
                const fieldName = discountTrigger.measurement && discountTrigger.measurement == 'amount' ? 'totalAmount' : 'totalUnit';
                const expressionString = (`${currentFormat[fieldName]}${discountTrigger.condition} ${discountTrigger.value}`);
                if (eval(expressionString)) {
                    const multiplier = Math.floor((currentFormat[fieldName] / parseInt(discountTrigger.value)) || 1);

                    discountResults.forEach((discountResult, indexData) => {
                        //handle changes of unit-value amount -re-calculate the total amount
                        if (discountResult.measurement == 'amount') {
                            currentFormat['totalAmount'] = discountResult.unitValue * currentFormat.totalUnit;
                        }

                        //handle changes of total -deduction or addition
                        if (discountResult.totalValue && parseInt(discountResult.totalValue) != 0) {
                            if (discountResult.measurement == 'unit') {
                                const chargeableUnit = eval(`${currentFormat['totalUnit']} ${discountResult.totalValue}*${multiplier}`)
                                //recalculate amount anytime unit changes
                                currentFormat['totalAmount'] = currentFormat.amount * chargeableUnit;
                            }
                            if (discountResult.measurement == 'amount') {
                                currentFormat['totalAmount'] = eval(`${currentFormat['totalAmount']} ${discountResult.totalValue}*${multiplier}`)
                            }
                        }
                    });
                }
            }
            this.discountEvaluator['currentFormat'] = currentFormat;
        };


    }
    initializeAfterDiscount() {
        const newList = [...this.listPurchase];
        newList.map(currentFormat => {
            const newCurrentFormat = Object.assign({}, currentFormat);
            this.discountEvaluator['initialize'](newCurrentFormat);
            const afterDiscountFormat = Object.assign({}, this.discountEvaluator['evaluate']());
            this.listPurchaseSummaryDiscount.totalAmounts += afterDiscountFormat.totalAmount;
            this.listPurchaseSummaryDiscount.totalUnits += afterDiscountFormat.totalUnit;
            this.listPurchaseDiscount.push(afterDiscountFormat);
        });
    }
    initializeBeforeDiscount() {
        this.currentCheckout = this.cartService.getCurrentCheckout();
        const currentSelectedItems = this.currentCheckout.items;

        if (currentSelectedItems) {
            //group based on sku
            currentSelectedItems.map(dataObj => {
                if (dataObj.skuCode && ProductEnum[dataObj.skuCode]) {
                    this.skuSelectionList[dataObj.skuCode] = this.skuSelectionList[dataObj.skuCode] || [];
                    this.skuSelectionList[dataObj.skuCode].push(dataObj);
                }
            });

            //add and squash each category   
            for (var keyName in this.skuSelectionList) {
                const skuName = keyName;
                const list = this.skuSelectionList[keyName];
                //unit
                const totalUnit = list.length;
                this.listPurchaseSummary.totalUnits += totalUnit;


                const currentFormat = new TransientFormat();
                currentFormat.skuCode = skuName;
                currentFormat.unit = 1;
                currentFormat.pricingRules = this.currentCheckout.pricingRules;

                //summary
                const totalAmount = list.reduce(function (sum, item) {
                    currentFormat.amount = item.price;
                    return sum + item.price;
                }, 0);

                this.listPurchaseSummary.totalAmounts += totalAmount;
                currentFormat.totalAmount = totalAmount;
                currentFormat.totalUnit = totalUnit;

                this.listPurchase.push(currentFormat)
            }
            console.log('this.listPurchase', this.listPurchase);

        }

    }
    payNow() {
        // alert('Payment Systems are under maintenance');
        this.isShowingInfo = true;
        this.showingCategory = 'payment';
    }
    getFriendlyDescription(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'description');
    }
    getFriendlyTitle(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'title');
    }
    getFriendlyLogo(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'logo');
    }

    buyMore() {
        location.href = "#/purchase"
    }



}

//transient format
export class TransientFormat {
    skuCode: string;
    unit: number;
    amount: number;
    totalUnit: number;
    totalAmount: number;
    pricingRules: PricingRule[];
}