import { NgModule } from '@angular/core';
import { PayComponent } from './pay.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const payRoutes: Routes = [
  {
    path: '',
    component: PayComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(payRoutes), CommonModule
  ],
  declarations: [
    PayComponent
  ],
  exports: [
    PayComponent
  ]
})
export class PayModule { }