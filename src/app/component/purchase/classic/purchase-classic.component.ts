

// Keep the Input import for now, we'll remove it later:
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';  
import { MessageService } from '../../../service/Message.service';
 
@Component({
    selector: 'purchase-classic-component',
    moduleId: module.id,
    templateUrl: './purchase-classic.component.html',
    styleUrls: ['purchase-classic.component.scss', '../../common.component.scss']
})
export class PurchaseClassicComponent implements OnInit {
    ngOnInit(): void { 
    }
    constructor(
        private messageService: MessageService, 
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {

        console.log('route route', route);

    }

 

    goBack(): void {
        this.location.back();
    }

 

    postNavigation(): void {
        this.location.back();
    } 
}
