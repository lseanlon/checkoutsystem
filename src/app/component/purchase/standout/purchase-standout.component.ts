
 
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router'; 
import { MessageService } from '../../../service/Message.service';

@Component({
    selector: 'purchase-standout-component',
    moduleId: module.id,
    templateUrl: './purchase-standout.component.html',
    styleUrls: ['purchase-standout.component.scss', '../../common.component.scss']
})
export class PurchaseStandoutComponent implements OnInit {
    ngOnInit(): void { 
    }
    constructor(
        private messageService: MessageService,  
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {

        console.log('route route', route);

    }
 
}
