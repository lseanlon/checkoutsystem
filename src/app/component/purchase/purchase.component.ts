

// Keep the Input import for now, we'll remove it later:
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/timer'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'

import { Router } from '@angular/router';
import { Purchase } from '../../class/Purchase';
import { PricingRule } from '../../class/Checkout/PricingRule';
import { Item } from '../../class/Checkout/Item';
import { Checkout } from '../../class/Checkout/Checkout';
import { MessageService } from '../../service/Message.service';
import { SessionService } from '../../service/Session.service';
import { ApiService } from '../../service/Api.service';
import { CartService } from '../../service/Cart.service';
import { ProductInfo } from '../../pipe/Product.pipe';


@Component({
    selector: 'purchase-component',
    moduleId: module.id,
    templateUrl: './purchase.component.html',
    styleUrls: ['purchase.component.scss', '../common.component.scss']
})
export class PurchaseComponent implements OnInit {

    salesList: Purchase[];
    discountList: PricingRule[];
    quantityInCart = [];
    countDown: any;
    counter: any = 32160;

    isShowingInfo: boolean = false;
    currentSkuCode: string;

    constructor(
        private messageService: MessageService,
        private sessionService: SessionService,
        private cartService: CartService,
        private apiService: ApiService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
        console.log('route route', route);

        this.initializeFakeTimer();
    }

    ngOnInit(): void {
        this.invokeMultipleService();

    }
    invokeMultipleService() {
        const currentUser = this.sessionService.getCurrentUser();

        const listOfServiceName = ['purchase', 'discount'];
        const listOfUrl = [`/purchase`, `/discount/${currentUser.username}`];

        this.apiService.showLoader();
        this.apiService.getMultipleData(listOfServiceName, listOfUrl).subscribe(listOfResults => {
            this.handleResponseList(listOfServiceName, listOfResults);
            this.apiService.hideLoader();
        }, error => {
            this.apiService.hideLoader();
        });
    }

    handleResponseList(listOfServiceName, listOfResults) {
        const fieldParameters = {};
        listOfResults.forEach((resultData, indexData) => {
            const serviceName = listOfServiceName[indexData];
            console.log('resultData', resultData);
            if (serviceName && resultData) {
                if (serviceName == 'discount') {
                    this.discountList = resultData;

                }
                if (serviceName == 'purchase') {
                    this.salesList = resultData;
                }

            }
        });
        this.initializeCart();

    }

    initializeCart() {
        this.cartService.initializePricingRule(this.discountList);
        this.cartService.initializeItem([]);
        this.getQuantityFromCart(null);

    }


    initializeFakeTimer() {
        this.countDown = Observable.timer(0, 1000)
            .take(this.counter)
            .map(() => --this.counter);
    }

    buyNow() {
        location.href = "#/pay";
    }

    displayMoreInfo(skuCode: string) {
        this.currentSkuCode = skuCode;
        this.isShowingInfo = true;
    }

    //cart related info
    addToCart(skuItem: Item) {
        const newItem = Object.assign({}, skuItem);
        this.cartService.addCurrentItem(newItem);
        this.getQuantityFromCart(skuItem.skuCode);
    }

    removeFromCart(skuCode: string) {
        this.cartService.removeCurrentItem(skuCode);
        this.getQuantityFromCart(skuCode);
    }

    getQuantityFromCart(skuCode: string) {
        const currentCheckout = Object.assign({}, this.cartService.getCurrentCheckout());

        //display how many sku in cart for all
        this.quantityInCart['*'] = currentCheckout.items.length;

        //filter based on skucode if exist 
        if (skuCode && currentCheckout.items) {
            currentCheckout.items = currentCheckout.items.filter(item => item.skuCode == skuCode);

            //display how many sku in cart for sku
            this.quantityInCart[skuCode] = currentCheckout.items.length;
        }

        (<any>document.getElementById('product-buy-button')).click();
    }




    //product-related info
    getFriendlyLogo(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'logo');
    }
    getFriendlyPreview(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'preview');
    }
    getFriendlyDescription(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'description');
    }
    getFriendlyTitle(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'title');
    }


    //discount-related info
    getCommonDiscount(skuCode: string, fieldName: string) {
        const discountSku = this.discountList.find(curElem => curElem.discountTrigger.skuCode === skuCode);

        if (discountSku && discountSku.discountTrigger) {
            return discountSku.discountTrigger[fieldName];
        }
        return '';
    }
    getDiscountInfo(skuCode: string) {
        return this.getCommonDiscount(skuCode, 'description');
    }
    getExpiryDate(skuCode: string) {
        return this.getCommonDiscount(skuCode, 'expiryDate');
    }
    setCountdownTimer(skuCode: string, endDate: string) {
        //@todo- countdown timer to encourage user buy
    }

}
