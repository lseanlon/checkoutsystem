import { NgModule } from '@angular/core';
import { PurchaseComponent } from './purchase.component';
import { PurchaseClassicComponent } from './classic/purchase-classic.component';
import { PurchaseStandoutComponent } from './standout/purchase-standout.component';
import { PurchasePremiumComponent } from './premium/purchase-premium.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
const purchaseRoutes: Routes = [
  {
    path: '',
    component: PurchaseComponent
  },
  {
    path: 'standout',
    component: PurchaseStandoutComponent
  },
  {
    path: 'classic',
    component: PurchaseClassicComponent
  },
  {
    path: 'premium',
    component: PurchaseClassicComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(purchaseRoutes), CommonModule
  ],
  declarations: [
    PurchaseComponent, PurchaseClassicComponent, PurchaseStandoutComponent, PurchasePremiumComponent
  ],
  exports: [
    PurchaseComponent, PurchaseClassicComponent, PurchaseStandoutComponent, PurchasePremiumComponent
  ]
})
export class PurchaseModule { }