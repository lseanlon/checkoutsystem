

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { APP_BASE_HREF } from '@angular/common';
import { NgForm } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Inject } from '@angular/core';
import {
    FormGroup,
    Validators,
    FormBuilder
} from '@angular/forms';

import { UserProfile } from '../../class/UserProfile';
import { SessionService } from '../../service/Session.service';

@Component({
    selector: 'login-outlet',
    moduleId: module.id,
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss', '../common.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    currentUser: UserProfile;
    userInfo: { userName, userPassword }
    constructor(private sessionService: SessionService, fb: FormBuilder) {
        this.loginForm = fb.group({
            userName: ["", Validators.required],
            userPassword: ["", Validators.required],
        });
    }

    ngOnInit(): void {
        this.performReset();
        this.loginForm.
            valueChanges.
            subscribe(form => { console.log('form', form); });

        this.performLogin();
    }

    performReset() {
        this.loginForm.controls["userName"].setValue('');
        this.loginForm.controls["userPassword"].setValue('');
        return false;
    }

    performLogin() {
        this.sessionService.performLogin(
            this.loginForm.controls["userName"].value,
            this.loginForm.controls["userPassword"].value
        );


    }


}
