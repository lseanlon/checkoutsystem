import { NgModule } from '@angular/core';
import { SubscriptionComponent } from './subscription.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const subscriptionRoutes: Routes = [
  {
    path: '',
    component: SubscriptionComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(subscriptionRoutes), CommonModule
  ],
  declarations: [
    SubscriptionComponent
  ],
  exports: [
    SubscriptionComponent
  ]
})
export class SubscriptionModule { }