

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';
import { Subscription } from '../../class/subscription';
import { SessionService } from '../../service/Session.service';
import { ProductInfo } from '../../pipe/Product.pipe';

@Component({
    selector: 'subscription-component',
    moduleId: module.id,
    templateUrl: './subscription.component.html',
    styleUrls: ['subscription.component.scss', '../common.component.scss']
})
export class SubscriptionComponent implements OnInit {

    subscriptionList: Subscription[];
    isShowingInfo:boolean = false;
    currentSkuCode:string; 

    constructor(
        private sessionService: SessionService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {

        console.log('route route', route);

    }
    ngOnInit(): void {
        this.subscriptionList = this.sessionService.getCurrentUser().subscriptionList;
    }

    getFriendlyPreview(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'preview');
    }
    getFriendlyDescription(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'description');
    }
    getFriendlyTitle(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'title');
    }
    getFriendlyLogo(skuCode: string) {
        return new ProductInfo().transform(skuCode, 'logo');
    }

    displayMoreInfo(skuCode:string) {
        this.currentSkuCode = skuCode;
        this.isShowingInfo = true ;
    }

    getRemainingDate(expiryDate:string) {

        const expDate = new Date(expiryDate);
        const curDate = new Date();

        const timeDiff = Math.abs(expDate.getTime() - curDate.getTime());
        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }




}
