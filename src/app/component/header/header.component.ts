

// Keep the Input import for now, we'll remove it later:
import { Component, Input, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';
import { MessageService } from '../../service/Message.service';
import { SessionService } from '../../service/Session.service';

@Component({
    selector: 'header-outlet',
    moduleId: module.id,
    templateUrl: './header.component.html',
    styleUrls: ['header.component.scss', '../common.component.scss']
})
export class HeaderComponent implements OnInit {


    theme: string;
    stickyStateClass: string = '';
    mainMenuStateClass: string = '';
    coreMenuList = [
        { "title": "Home", "url": "#/", "children": [], "classState": "selected", "category": "" },
        { "title": "About", "url": "#/", "children": [], "classState": "selected", "category": "pre-login" },
        { "title": "Login", "url": "#/login", "children": [], "classState": "selected", "category": "pre-login" },
        { "title": "Subcription", "url": "#/subscription", "children": [], "classState": "not-selected", "category": "post-login" },
        {
            "title": "Purchase", "category": "post-login", "url": "#/purchase", "children": [

                { "title": "Pick", "url": "#/purchase", "classState": "selected" },
                { "title": "Pay", "url": "#/pay", "classState": "not-selected" }
            ],
            "classState": "not-selected"
        },
        { "title": "Profile", "url": "#/profile", "category": "post-login", "children": [], "classState": "not-selected" },
        { "title": "Logout", "url": "#/logout", "category": "post-login", "children": [], "classState": "not-selected" },
    ]
    menuList = [];

    constructor(
        private messageService: MessageService,
        private sessionService: SessionService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {

        console.log('route route', route);

    }

    ngOnInit(): void {
        this.subscribeLoginState();
        this.filterMenu('pre-login');
    }

    @HostListener('window:scroll', ['$event'])
    doSomething(event) {
        // console.debug("Scroll Event", document.body.scrollTop); 
        console.log("Scroll Event", window.pageYOffset);
        if (window.pageYOffset >= 10) {
            this.stickyStateClass = 'sticky';
        } else {
            this.stickyStateClass = '';
        }
    }
    subscribeLoginState() {
        this.messageService.messageAnnounced$.subscribe(currentState => {
            this.filterMenu(currentState);
            console.log('currentState', currentState);
        });

    }
    filterMenu(currentState) {

        this.menuList = this.coreMenuList.filter(item =>
            !item.category || (currentState && item.category == currentState)
        );
    }
    performMainMenuToggle(): void {
        this.mainMenuStateClass = this.mainMenuStateClass == 'clicked' ? '' : 'clicked';
    }

    performContentMenuToggle(menuItem): void {
        this.performCommonToggle(false, this.menuList, menuItem);
    }

    performChildMenuToggle(childList, menuItem): void {
        this.performCommonToggle(true, childList, menuItem);
    }

    performCommonToggle(isChild, menuList, menuItem) {

        //deselect all
        menuList.map(dataObj => {
            dataObj.classState = "not-selected";
            return dataObj;
        });

        //select sepecifc one
        const indexKey = menuList.findIndex(curElem => curElem.title === menuItem.title);
        menuList[indexKey].classState = "selected";

        if (isChild) {
            //close current box
            this.performMainMenuToggle()
        } else {
            //only when select lowest child
            if (!menuItem.children.length) {
                //close current box
                this.performMainMenuToggle()
            }
        }



    }

    goBack(): void {
        this.location.back();
    }




    postNavigation(): void {
        this.location.back();
    }
}
