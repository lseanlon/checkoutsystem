// Keep the Input import for now, we'll remove it later:
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';
import { UserProfile } from '../../class/UserProfile';
import { MessageService } from '../../service/Message.service';
import { SessionService } from '../../service/Session.service';

@Component({
    selector: 'profile-component',
    moduleId: module.id,
    templateUrl: './profile.component.html',
    styleUrls: ['profile.component.scss', '../common.component.scss']
})
export class ProfileComponent implements OnInit {

    currentUser: UserProfile;
    ngOnInit(): void {
        this.currentUser = this.sessionService.getCurrentUser();
    }
    constructor(
        private sessionService: SessionService,
        private messageService: MessageService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
 

    }




}
