

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';

import { Router } from '@angular/router';
import { SessionService } from '../../service/Session.service';

@Component({
    selector: 'logout-component',
    moduleId: module.id,
    templateUrl: './logout.component.html',
    styleUrls: ['logout.component.scss', '../common.component.scss']
})
export class LogoutComponent implements OnInit {

    currentTime: string;

    ngOnInit(): void {
        const moment = require('moment')
        this.currentTime = moment(new Date()).format('YYYY-MM-DD hh:mm:ss');
        this.sessionService.performLogout();
        this.sessionService.setCurrentUser(null);
    }
    constructor(
        private sessionService: SessionService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
    ) {
    }


}
