import {NgModule} from '@angular/core';
import {LogoutComponent} from './logout.component';
import {Routes, RouterModule} from '@angular/router';

const logoutRoutes:Routes = [
  {
    path: '',
    component: LogoutComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(logoutRoutes)
  ],
  declarations: [
    LogoutComponent
  ],
  exports: [
    LogoutComponent
  ]
})
export class LogoutModule {}