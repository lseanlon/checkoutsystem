import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' }, 
    { path: 'login', loadChildren: './component/login/login.module#LoginModule' }, 
    { path: 'logout', loadChildren: './component/logout/logout.module#LogoutModule' },  
    { path: 'profile', loadChildren: './component/profile/profile.module#ProfileModule' },
    { path: 'purchase', loadChildren: './component/purchase/purchase.module#PurchaseModule' },
    { path: 'subscription', loadChildren: './component/subscription/subscription.module#SubscriptionModule' },
    { path: 'pay', loadChildren: './component/pay/pay.module#PayModule' },
 

];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
