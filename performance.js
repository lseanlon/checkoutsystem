const psi = require('psi');

const url = 'theverge.com'
const ngrok = require('ngrok');
ngrok.connect(4200, function (err, url) {


    // Get the PageSpeed Insights report 
    psi(url).then(data => {
        console.log(data.ruleGroups.SPEED.score);
        console.log(data.pageStats);
    });

    // Output a formatted report to the terminal 
    psi.output(url).then(() => {
        console.log('done');
    });

    // Supply options to PSI and get back speed and usability scores 
    psi(url, { nokey: 'true', strategy: 'mobile' }).then(data => {
        console.log('Speed score:', data.ruleGroups.SPEED.score);
        console.log('Usability score:', data.ruleGroups.USABILITY.score);
    });

}); 