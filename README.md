 
##Features
Environment bundling pick and match assets building for release ->eg. prod,dev,preprod,ci
Separate configuration for different environments
Version stamping
External deployment-independent api configuration
Mock server
Test Case builtin 
Performance test builtin with ngrok and psi
Lazy loading
Cross-component communication via observer
Proper structure enum ,classes
Computated doc -compodoc


##Installation and Running 
npm Install

#Development Mode
npm run development
npm run mock-storage-api



#Production Mode
npm run production

#performance Mode
npm run performance
#doc Mode
npm run doc

