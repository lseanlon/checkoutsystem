
const ANGULAR_CONFIG_FILE = './angular-cli.json';
const API_CONFIG_FILE = `./src/assets/data/api-config.json`;
const DEFAULT_ENVIRONMENT = 'dev';
const ALLOWED_VALUES = ['', 'dev', 'ci', 'pp', 'prod'];

const replace = require('replace-in-file');
const package = require('./package.json');
const git = require('git-rev');
const _ = require('lodash');
const fs = require('fs');
const angularConfig = require(ANGULAR_CONFIG_FILE);


let targetFileValue = '';
let targetFile = '';
let angularConfigString = '';

function initializeParameter() {

    console.log('initializeParameter \n');
    //access parameter node
    process.argv.forEach(function (val, index, array) {
        //overwrite target env

        if (index == 2 && val) {
            const envTargetValue = val + '' || DEFAULT_ENVIRONMENT;
            targetFileValue = envTargetValue;
        }

    });

    console.log('initializeParameter targetFile', targetFile);
    console.log('initializeParameter targetFileValue', targetFileValue);
}


function updateConfigurationAssets() {
    console.log('updateConfigurationAssets  \n');

    ALLOWED_VALUES.forEach((itemVal) => {
        console.log('\nitemVal', itemVal);
        if (!itemVal) {
            targetFile = 'src/environments/environment.ts';
        } else {
            targetFile = `src/environments/environment.${itemVal}.ts`;
        }


        //overwrite targetFileValue enivronment into file
        const options = {
            files: targetFile,
            from: /targetEnvironment: "[A-z0-9]+", /,
            to: `targetEnvironment: "${targetFileValue}",`,
            allowEmptyPaths: false,
        };

        try {
            console.log('build targetEnvironmentValue set: ', targetFileValue);
            console.log('build targetEnvironment set: ', targetFile);
            let changedFiles = replace.sync(options);
            if (changedFiles == 0) {
                console.log('no changed applied ');
            }
        } catch (error) {
            console.error('error occurred:', error);
            throw error
        }


    });

    //packaging configuration - overwrite asset configuration  
    console.log(`updating angularconfig ${ANGULAR_CONFIG_FILE} ...`);
    const newConfig = _.cloneDeep(angularConfig);
    const assetsValue = require(`./configuration/assets/assets-${targetFileValue}.json`);
    newConfig['apps'][0]['assets'] = (assetsValue);
    angularConfigString = JSON.stringify(newConfig  ,null, '\t');
  
    // console.log("angularConfig ", angularConfigString);


    console.log("\nupdating angular-cli file...", ANGULAR_CONFIG_FILE);
    console.log("... with new value ", angularConfigString);
    fs.writeFileSync(ANGULAR_CONFIG_FILE, angularConfigString, 'utf8');

}


function updateConfigurationApi() {
    console.log('\n updateConfigurationApi  \n');
    //get api file based on target 
    const sourceApiValue = require(`./configuration/api/api-config-${targetFileValue}.json`);
    const newSourceApiValue = _.cloneDeep(sourceApiValue);
    const apiConfigString = JSON.stringify(newSourceApiValue,  null, '\t');

    //update api-config json
    console.log("\nupdating API file...", API_CONFIG_FILE);
    console.log("... with new value ", apiConfigString);
    fs.writeFileSync(API_CONFIG_FILE, apiConfigString, 'utf8');


}


try {
    initializeParameter();
    updateConfigurationAssets();
    updateConfigurationApi();

} catch (error) {
    console.error('Error occurred:', error);
    throw error
}
